Definitions.

ESPACIOS =  [\s\t]
ENTERS =  \n|\r\n|\r
NUM =  [0-9]
DIGITO =  [1-9]
MENOS =  [\-]
FRACCION =  \.{NUM}+
INICORCHETE =  \[
FINCORCHETE =  \]
INILLAVE =  \{
FINLLAVE =  \}
DOBLEPUNTO =  :
TRUE  =  true
FALSE =  false
NULL =  null
COMMA =  ,
FUNCIONINT = {MENOS}?0|{MENOS}?{DIGITO}{NUM}*
VALORINT = {FUNCIONINT}
VALORFLOAT =  {FUNCIONINT}{FRACCION}|{FUNCIONINT}
STRING = "([^\"\\]|\\.)*"

Rules.


{ESPACIOS} :  skip_token.
{COMMA} :  atom_token(comma, TokenLine, TokenChars).
{INILLAVE} :  atom_token(inicio_llave, TokenLine, TokenChars).
{FINLLAVE} :  atom_token(fin_llave, TokenLine, TokenChars).
{INICORCHETE} :  atom_token(inicio_corchete, TokenLine, TokenChars).
{FINCORCHETE} :  atom_token(fin_corchete, TokenLine, TokenChars).
{DOBLEPUNTO} :  atom_token(dolbe_punto, TokenLine, TokenChars).
{VALORINT} :  {token, {int, TokenLine, list_to_integer(TokenChars)}}.
{VALORFLOAT} :  {token, {float, TokenLine, list_to_float(TokenChars)}}.
{STRING} : {token, {string, TokenLine, list_to_binary(string:trim(TokenChars, both, "\""))}}.
{TRUE} :  {token, {bool, TokenLine, true}}.
{FALSE} :  {token, {bool, TokenLine, false}}.
{NULL} : {token, {null, TokenLine, nil}}.
{ENTERS} : {token, {enters, TokenLine, TokenChars}}.


Erlang code.
atom_token(Atom, TokenLine, TokenChars) ->
    {token, {Atom, TokenLine, list_to_atom(TokenChars)}}.
